<div align="center">
  <img src=".gitlab/logo.svg" height="124px"/><br/>
  <h1>YACSP</h1>
  <p>Backend for Yet Another Code Sharing Project</p>
</div>




## 📝 About The Project

The project helps to share code between developers

## ⚡️ Quick start

First of all, make sure you have [installed](https://www.python.org/downloads/) **Python**. Version `3.10` or higher is required.

Download the repository and change the current directory:

```bash
git clone git@gitlab.com:markovvn1-iu/s23-sqr/group-project/backend.git && cd backend
```

Configure virtual environment. Make sure a `.venv` folder has been created after this step.

```bash
make venv
```

Run the project:

```bash
GOOGLE_RECAPTCHA_SERVER_TOKEN="token" make up
```

Open the website `http://localhost:8000/`.

### :whale: Docker-way to quick start

Run the container:

```bash
docker run --rm -it -p 80:80 registry.gitlab.com/markovvn1-iu/s23-sqr/group-project/backend:latest
```

Open the website `http://localhost/`.

## ⚙️ Developing

Configure virtual environment. Make sure a `.venv` folder has been created after this step.

```bash
make venv
# source .venv/bin/activate
```

Run application locally (in debug mode):

```bash
make up
```

Run linters, formaters and tests:

```bash
make lint  # check code quality
make format  # beautify code
make test  # run unit and integrated tests
```

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i> <br> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>v.markov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>

  :boy: <b>Ilya Kolomin</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>i.kolomin@innopolis.university</a> <br>

  :boy: <b>Kirill Ivanov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>k.ivanov@innopolis.university</a> <br>

  :boy: <b>Lev Lymarenko</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>l.lymarenko@innopolis.university</a> <br>

  :girl: <b>Polina Minina</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>p.minina@innopolis.university</a> <br>
</p>

## :scroll: License

`Swipio backend` is free and open-source software licensed under the [MIL License](LICENSE)