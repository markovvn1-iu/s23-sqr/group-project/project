import asyncio

from yacsp.core import AppEvent
from yacsp.services import FileUIDService


async def main() -> None:
    service = FileUIDService.get_instance()
    uid = service.id2uid(726009797)
    print(uid)
    print(service.uid2id(uid))


if __name__ == "__main__":

    async def main_wrap() -> None:
        await asyncio.gather(*[i.init() for i in AppEvent.__subclasses__()])
        try:
            await main()
        finally:
            await asyncio.gather(*[i.deinit() for i in AppEvent.__subclasses__()])

    asyncio.run(main_wrap())
