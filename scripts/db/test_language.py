import asyncio

from yacsp.core import AppEvent
from yacsp.db.repositories import LanguageRepository


async def main() -> None:
    with LanguageRepository.create() as repo:
        repo.add_if_not_exist("C++")
        repo.add_if_not_exist("Python")
        print(repo.get_all())


if __name__ == "__main__":

    async def main_wrap() -> None:
        await asyncio.gather(*[i.init() for i in AppEvent.__subclasses__()])
        try:
            await main()
        finally:
            await asyncio.gather(*[i.deinit() for i in AppEvent.__subclasses__()])

    asyncio.run(main_wrap())
