from pydantic import BaseModel

from yacsp.utils.str_enum_with_id import StrEnumWithId


class MyEnum(StrEnumWithId):
    VALUE1 = "value1", 0
    VALUE2 = "value2", 1


class MyModel(BaseModel):
    enum: MyEnum


print(list(MyEnum))
print(MyEnum("value1").id)
print(MyModel(enum=MyEnum.VALUE2).json())
print(MyModel.parse_raw(MyModel(enum=MyEnum.VALUE2).json()).enum.id)
