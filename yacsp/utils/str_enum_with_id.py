from enum import Enum
from typing import Any, Type, TypeVar

T = TypeVar("T", bound="StrEnumWithId")


class StrEnumWithId(Enum):
    """Derive from this class to define new enumerations with ids.

    Usage example:
    class MyEnum(StrEnumWithId):
        VALUE1 = "value1", 0
        VALUE2 = "value2", 1

    print(MyEnum.VALUE1.value, MyEnum.VALUE1.id)
    """

    def __new__(cls: Type[T], *args: Any, **_kwargs: Any) -> T:
        obj = object.__new__(cls)
        obj._value_ = args[0]
        id2enum: dict[int, T] = getattr(cls, "_id2enum", None) or {}
        id2enum[args[1]] = obj
        cls._id2enum = id2enum
        return obj

    def __init__(self, *args: Any) -> None:
        assert len(args) == 2
        self._id_ = args[1]

    @classmethod
    def from_id(cls: Type[T], _id: int) -> T:
        assert isinstance(cls._id2enum, dict)
        return cls._id2enum[_id]

    @property
    def id(self) -> int:
        return self._id_
