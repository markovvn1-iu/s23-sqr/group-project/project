from typing import Callable, Generator, Type, TypeVar

from yacsp.db.repositories import BaseRepository

RepositoryType = TypeVar("RepositoryType", bound=BaseRepository)


def get_repository(repo_type: Type[RepositoryType]) -> Callable[[], Generator[RepositoryType, None, None]]:
    def wrap() -> Generator[RepositoryType, None, None]:
        with repo_type.create() as repo:
            yield repo

    return wrap
