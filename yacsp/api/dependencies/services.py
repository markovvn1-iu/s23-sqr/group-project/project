from typing import Callable, Type, TypeVar

from yacsp.services.base import BaseService

ServiceType = TypeVar("ServiceType", bound=BaseService)


def get_service(service_type: Type[ServiceType]) -> Callable[[], ServiceType]:
    def wrap() -> ServiceType:
        return service_type.get_instance()

    return wrap
