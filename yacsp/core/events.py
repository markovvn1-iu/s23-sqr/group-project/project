class AppEvent:
    @classmethod
    async def init(cls) -> None:
        pass

    @classmethod
    async def deinit(cls) -> None:
        pass
