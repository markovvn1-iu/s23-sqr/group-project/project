from yacsp.db.exceptions import NotFoundRepoException
from yacsp.db.models import ParamsDB

from .base import BaseRepository


class ParamsRepository(BaseRepository):
    def get(self, name: str) -> str:
        res = self._sess.query(ParamsDB.value).filter_by(name=name).first()
        if res is None or res[0] is None:
            raise NotFoundRepoException(f"Parameter with name '{name}' do not found")
        return res[0]

    def set(self, name: str, value: str) -> None:
        self._sess.merge(ParamsDB(name=name, value=value))
        self._sess.commit()
