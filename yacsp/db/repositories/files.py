from sqlalchemy import desc, func

from yacsp.core import AppEvent
from yacsp.db.exceptions import NotFoundRepoException
from yacsp.models import FileInResponse, FileInResponseWithId, FileMark, FileVisibility, Language, NewFileInRequest

from ..models import FileDB, LanguageDB, MarkDB
from .base import BaseRepository


class FilesRepository(BaseRepository, AppEvent):
    def get_file_for_response(self, file_id: int, file_uid: str, user_id: int) -> FileInResponse:
        res: tuple[FileDB, LanguageDB, int | None] | None = (
            self._sess.query(FileDB, LanguageDB, func.coalesce(func.sum(MarkDB.mark), 0).label("total_mark"))
            .filter_by(file_id=file_id)
            .join(LanguageDB, isouter=True)
            .join(MarkDB, isouter=True)
            .first()
        )
        if res is None or res[0] is None:
            raise NotFoundRepoException(f"File with file_id '{file_id}' is not found")
        file, language, total_mark = res
        if language is None:
            raise NotFoundRepoException(f"Language not found for file with id '{file_id}'")

        mark = self._sess.query(MarkDB).filter_by(user_id=user_id, file_id=file_id).first()
        user_mark = FileMark.NO_STAR
        if mark is not None:
            user_mark = FileMark.from_id(mark.mark)

        return FileInResponse(
            uid=file_uid,
            title=file.title,
            content=file.content.decode(),
            language=Language(id=language.language_id, name=language.name),
            visibility=FileVisibility.from_id(file.visibility),
            mark=user_mark,
            star_count=total_mark,
        )

    def save_file(self, file: NewFileInRequest) -> int:
        lang = self._sess.query(LanguageDB).filter_by(language_id=file.language_id).first()
        if lang is None:
            raise NotFoundRepoException(f"Language with id = {file.language_id} does not exist")
        fileDb = FileDB(
            title=file.title,
            content=file.content.encode(),
            language_id=file.language_id,
            visibility=file.visibility.id,
        )
        self._sess.add(fileDb)
        self._sess.commit()
        return fileDb.file_id

    def get_top_files(self, user_id: int, offset: int = 0, limit: int = 10) -> list[FileInResponseWithId]:
        res: list[tuple[FileDB, LanguageDB, int]] = (
            self._sess.query(FileDB, LanguageDB, func.coalesce(func.sum(MarkDB.mark), 0).label("total_mark"))
            .filter_by(visibility=FileVisibility.PUBLIC.id)
            .join(LanguageDB, isouter=True)
            .join(MarkDB, isouter=True)
            .group_by(FileDB.file_id)
            .order_by(desc("total_mark"))
            .limit(limit)
            .offset(offset)
            .all()
        )
        files_response = []
        for i in res:
            file, language, total_mark = i

            mark = self._sess.query(MarkDB).filter_by(user_id=user_id, file_id=file.file_id).first()
            user_mark = FileMark.NO_STAR
            if mark is not None:
                user_mark = FileMark.from_id(mark.mark)

            files_response.append(
                FileInResponseWithId(
                    id=file.file_id,
                    title=file.title,
                    content=file.content.decode(),
                    language=Language(id=language.language_id, name=language.name),
                    visibility=FileVisibility.from_id(file.visibility),
                    mark=user_mark,
                    star_count=total_mark,
                )
            )
        return files_response
