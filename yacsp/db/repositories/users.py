from ..models import UserDB
from .base import BaseRepository


class UsersRepository(BaseRepository):
    def create_user(self, remote_ip_address: str) -> int:
        user = UserDB(remote_ip_address=remote_ip_address)
        self._sess.add(user)
        self._sess.commit()
        return user.user_id
