import json

from yacsp.core import AppEvent
from yacsp.db.models import LanguageDB
from yacsp.models import Language
from yacsp.project import STORAGE_PATH

from .base import BaseRepository


class LanguageRepository(BaseRepository, AppEvent):
    def get_all(self) -> list[Language]:
        res = self._sess.query(LanguageDB).all()
        return [Language(id=i.language_id, name=i.name) for i in res]

    def add_if_not_exist(self, name: str) -> int:
        res = self._sess.query(LanguageDB.language_id).filter_by(name=name).first()
        if res is not None:
            return res[0]
        language = LanguageDB(name=name)
        self._sess.add(language)
        self._sess.commit()
        return language.language_id

    @classmethod
    async def init(cls) -> None:
        languages_file = STORAGE_PATH / "languages.json"
        if not languages_file.exists():
            with open(languages_file, "w", encoding="utf-8") as f:
                json.dump([], f)
        with open(languages_file, "r", encoding="utf-8") as f:
            languages = json.load(f)
        with LanguageRepository.create() as repo:
            for lang in languages:
                repo.add_if_not_exist(lang)
