from contextlib import contextmanager
from typing import Generator, Type, TypeVar

from sqlalchemy.orm import Session

from yacsp.db import YACSPDatabase

T = TypeVar("T", bound="BaseRepository")


class BaseRepository:
    _sess: Session

    def __init__(self, sess: Session) -> None:
        self._sess = sess

    @classmethod
    @contextmanager
    def create(cls: Type[T]) -> Generator[T, None, None]:
        with YACSPDatabase.get_instance().get_session() as sess:
            try:
                yield cls(sess)
                sess.commit()
            except BaseException:
                sess.rollback()
                raise
