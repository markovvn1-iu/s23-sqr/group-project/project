from typing import ContextManager, Optional, Type

from loguru import logger
from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.orm import Session, sessionmaker

from yacsp.core import AppEvent
from yacsp.project import STORAGE_PATH

from .models import SQLBase


class YACSPDatabase(AppEvent):
    DATABASE_URI = f"sqlite:///{STORAGE_PATH / 'storage.db'}"

    _engine: Engine
    _make_session: sessionmaker
    __instance: Optional["YACSPDatabase"] = None

    @classmethod
    async def init(cls) -> None:
        cls.get_instance()

    @classmethod
    async def deinit(cls) -> None:
        cls.__instance = None

    @classmethod
    def get_instance(cls: Type["YACSPDatabase"]) -> "YACSPDatabase":
        if cls.__instance is not None:
            return cls.__instance
        STORAGE_PATH.mkdir(exist_ok=True)
        cls.__instance = cls(cls.DATABASE_URI)
        return cls.__instance

    def __init__(self, database_url: str) -> None:
        if self.__instance is not None:
            raise RuntimeError("Class YACSPDatabase is a singleton!")

        logger.debug("Creating YACSPDatabase (database_url='{}')", database_url)
        self._engine = create_engine(database_url, connect_args={"check_same_thread": False})  # , echo=True
        self._make_session = sessionmaker(autocommit=False, autoflush=False, bind=self._engine)
        SQLBase.metadata.create_all(bind=self._engine)

    def get_session(self) -> ContextManager[Session]:
        return self._make_session()
