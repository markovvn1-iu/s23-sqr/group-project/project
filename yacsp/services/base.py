from abc import ABC, abstractmethod
from typing import Type, TypeVar

T = TypeVar("T", bound="BaseService")


class BaseService(ABC):
    @classmethod
    @abstractmethod
    def get_instance(cls: Type[T]) -> T:
        pass
