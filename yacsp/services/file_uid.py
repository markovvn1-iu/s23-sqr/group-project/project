import base64
import random
import secrets
import zlib
from dataclasses import dataclass
from typing import Optional

from loguru import logger

from yacsp.core import AppEvent
from yacsp.db.exceptions import NotFoundRepoException
from yacsp.db.repositories import ParamsRepository

from .base import BaseService


@dataclass
class FileUIDLocalKey:
    xor: int
    perm: list[int]

    @property
    def rperm(self) -> list[int]:
        return self._rperm

    def __init__(self, xor: int, perm: list[int]) -> None:
        assert len(set(perm)) == len(perm)
        self.xor = xor
        self.perm = perm
        self._rperm = [0] * len(perm)
        for i, it in enumerate(perm):
            self._rperm[it] = i


@dataclass
class FileUIDKey(FileUIDLocalKey):
    local: list[FileUIDLocalKey]

    def __init__(self, xor: int, perm: list[int], local: list[FileUIDLocalKey]) -> None:
        super().__init__(xor, perm)
        self.local = local


class FileUIDServiceException(Exception):
    pass


class BadFileUIDKey(FileUIDServiceException):
    pass


class BadFileUID(FileUIDServiceException):
    pass


class FileUIDService(BaseService, AppEvent):
    __instance: Optional["FileUIDService"] = None

    _secret_key: FileUIDKey

    def __init__(self, secret_key: FileUIDKey):
        self._secret_key = secret_key

    @staticmethod
    def _generate_secret_key() -> FileUIDKey:
        local_keys: list[FileUIDLocalKey] = []
        for _ in range(256):
            p = list(range(8 * 5))
            random.shuffle(p)
            local_keys.append(FileUIDLocalKey(secrets.randbits(5 * 8), p))
        p = list(range(8 * 6))
        random.shuffle(p)
        return FileUIDKey(secrets.randbits(6 * 8), p, local_keys)

    @staticmethod
    def _encode_key(key: FileUIDKey) -> str:
        res = []
        res.append(key.xor.to_bytes(6, byteorder="little"))
        res.append(bytes(key.perm))
        for local_key in key.local:
            res.append(local_key.xor.to_bytes(5, byteorder="little"))
            res.append(bytes(local_key.perm))
        return base64.b64encode(zlib.compress(b"".join(res))).decode()

    @staticmethod
    def _decode_key(key: str) -> FileUIDKey:
        try:
            bkey = zlib.decompress(base64.b64decode(key))
        except Exception as e:
            raise BadFileUIDKey("Failed to decode FileUIDKey") from e

        if len(bkey) != 6 + 8 * 6 + (5 + 8 * 5) * 256:
            raise BadFileUIDKey("Failed to decode FileUIDKey: incorrect length")

        key_xor = int.from_bytes(bkey[:6], byteorder="little")
        key_perm = list(bkey[6 : 6 + 8 * 6])
        bkey = bkey[6 + 8 * 6 :]

        local_keys: list[FileUIDLocalKey] = []
        for i in range(0, len(bkey), 5 + 5 * 8):
            local_key_xor = int.from_bytes(bkey[i : i + 5], byteorder="little")
            local_key_perm = list(bkey[i + 5 : i + 5 + 5 * 8])
            local_keys.append(FileUIDLocalKey(local_key_xor, local_key_perm))

        return FileUIDKey(key_xor, key_perm, local_keys)

    @classmethod
    def _get_or_generate_secret_key(cls, params_repo: ParamsRepository) -> FileUIDKey:
        try:
            sres = params_repo.get("file_uid_secret_key")
            logger.info("FileUID secret key loaded")
            return cls._decode_key(sres)
        except NotFoundRepoException:
            res = cls._generate_secret_key()
            params_repo.set("file_uid_secret_key", cls._encode_key(res))
            logger.info("New FileUID secret key gererated")
            return res

    @classmethod
    async def init(cls) -> None:
        cls.get_instance()

    @classmethod
    async def deinit(cls) -> None:
        if cls.__instance is not None:
            cls.__instance = None

    @classmethod
    def get_instance(cls) -> "FileUIDService":
        if cls.__instance is None:
            with ParamsRepository.create() as params_repo:
                secret_key = cls._get_or_generate_secret_key(params_repo)
            cls.__instance = cls(secret_key)
        return cls.__instance

    def id2uid(self, _id: int) -> str:
        if (_id < 0) or _id >= (1 << 32):
            raise ValueError("id is out of range")
        checksum = (_id & 255) ^ ((_id >> 8) & 255) ^ ((_id >> 16) & 255) ^ ((_id >> 24) & 255)
        checksum_bits = [(checksum >> i) & 1 for i in range(8)]
        key, lkey = self._secret_key, self._secret_key.local[checksum]
        res = [(_id >> i) & 1 for i in range(32)] + checksum_bits
        assert len(lkey.perm) == len(res)
        res = [res[lkey.perm[i]] ^ ((lkey.xor >> i) & 1) for i in range(len(res))] + checksum_bits
        assert len(key.perm) == len(res)
        res = [res[key.perm[i]] ^ ((key.xor >> i) & 1) for i in range(len(res))]
        uid_bytes = [sum(res[i + j] << j for j in range(8)) for i in range(0, len(res), 8)]
        uid = base64.urlsafe_b64encode(bytes(uid_bytes)).decode()
        assert len(uid) == 8
        return uid

    def uid2id(self, uid: str) -> int:
        if len(uid) != 8:
            raise BadFileUID("Incorrect uid length")
        try:
            uid_bytes = base64.urlsafe_b64decode(uid.encode())
        except Exception as e:
            raise BadFileUID("Uid not in base64 urlsafe format") from e
        key = self._secret_key
        res = [(i >> j) & 1 for i in uid_bytes for j in range(8)]
        assert len(res) == len(key.rperm)
        res = [res[key.rperm[i]] ^ ((key.xor >> key.rperm[i]) & 1) for i in range(len(res))]
        checksum_bits = res[-8:]
        checksum = sum(checksum_bits[j] << j for j in range(8))
        lkey = key.local[checksum]
        assert len(res) == len(lkey.rperm) + 8
        res = [res[lkey.rperm[i]] ^ ((lkey.xor >> lkey.rperm[i]) & 1) for i in range(len(lkey.rperm))]
        if res[-8:] != checksum_bits:
            raise BadFileUID("Incorrect checksum")
        _id = sum(res[j] << j for j in range(32))
        if (_id & 255) ^ ((_id >> 8) & 255) ^ ((_id >> 16) & 255) ^ ((_id >> 24) & 255) != checksum:
            raise BadFileUID("Incorrect checksum")
        return _id
