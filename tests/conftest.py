import os
from typing import AsyncGenerator

import pytest
from fastapi.testclient import TestClient

from yacsp.api.dependencies.authentication import get_current_user_id
from yacsp.db import YACSPDatabase
from yacsp.db.repositories import LanguageRepository, MarksRepository
from yacsp.db.repositories.files import FilesRepository
from yacsp.main import app
from yacsp.settings import Settings

Settings._init(Settings.SettingsEnv(google_recaptcha_server_token=""))


@pytest.fixture()
async def _init_db() -> AsyncGenerator[YACSPDatabase, None]:
    if os.path.isfile("storage_for_test.db"):
        os.remove("storage_for_test.db")
    YACSPDatabase.DATABASE_URI = "sqlite:///storage_for_test.db"
    await YACSPDatabase.init()
    yield YACSPDatabase.get_instance()
    await YACSPDatabase.deinit()
    if os.path.isfile("storage_for_test.db"):
        os.remove("storage_for_test.db")


@pytest.fixture(name="language_repo")
async def _language_repo(_init_db: YACSPDatabase) -> AsyncGenerator[LanguageRepository, None]:
    # await LanguageRepository.init()
    with LanguageRepository.create() as repo:
        yield repo
    # await LanguageRepository.deinit()


@pytest.fixture(name="files_repo")
async def _files_repo(_init_db: YACSPDatabase) -> AsyncGenerator[FilesRepository, None]:
    # await FilesRepository.init()
    with FilesRepository.create() as repo:
        yield repo
    # await FilesRepository.deinit()


@pytest.fixture(name="client")
async def _client(_init_db: YACSPDatabase) -> TestClient:
    app.dependency_overrides[get_current_user_id] = lambda: 0
    client = TestClient(app)
    return client


@pytest.fixture(name="marks_repo")
async def _marks_repo(_init_db: YACSPDatabase) -> AsyncGenerator[MarksRepository, None]:
    with MarksRepository.create() as repo:
        yield repo
