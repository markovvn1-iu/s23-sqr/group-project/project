import json
from fastapi.testclient import TestClient

from yacsp.db.repositories import LanguageRepository
from yacsp.models import (
    FileInResponse,
    FileMark,
    FileVisibility,
    NewFileCreatedResponse,
    NewFileInRequest,
    NewMarkRequest,
)


def test_store_and_load_file(client: TestClient, language_repo: LanguageRepository) -> None:
    language_id = language_repo.add_if_not_exist("Python")
    file = NewFileInRequest(
        title="test title", content="test content", language_id=language_id, visibility=FileVisibility.PUBLIC
    )
    response = client.post("/api/file/new", content=file.json())
    assert response.status_code == 201
    file_uid = NewFileCreatedResponse.parse_raw(response.content).uid

    response = client.get(f"/api/file/{file_uid}")
    assert response.status_code == 200
    loaded_file = FileInResponse.parse_raw(response.content)

    assert loaded_file.uid == file_uid
    assert NewFileInRequest(**loaded_file.dict(), language_id=loaded_file.language.id) == file


def test_put_mark(client: TestClient, language_repo: LanguageRepository) -> None:
    language_id = language_repo.add_if_not_exist("Python")
    file = NewFileInRequest(
        title="test title", content="test content", language_id=language_id, visibility=FileVisibility.PUBLIC
    )
    response = client.post("/api/file/new", content=file.json())
    assert response.status_code == 201
    file_uid = NewFileCreatedResponse.parse_raw(response.content).uid

    mark = NewMarkRequest(file_uid=file_uid, new_mark=FileMark.STAR)
    response = client.put(f"/api/file/{file_uid}/mark", content=mark.json())

    assert response.status_code == 200

    response = client.get(f"/api/file/{file_uid}")
    assert response.status_code == 200
    loaded_file = FileInResponse.parse_raw(response.content)

    assert loaded_file.uid == file_uid
    assert NewFileInRequest(**loaded_file.dict(), language_id=loaded_file.language.id) == file
    assert loaded_file.mark == FileMark.STAR
    assert loaded_file.star_count == 1


def test_empty_top(client: TestClient) -> None:
    response = client.get("/api/top", params={'offset': 0, 'limit': 0})
    assert response.status_code == 200
    assert json.loads(response.content) == []


def test_top_without_marks(client: TestClient, language_repo: LanguageRepository) -> None:
    response = client.get("/api/top", params={'offset': 0, 'limit': 0})
    assert response.status_code == 200
    assert json.loads(response.content) == []

    language_id = language_repo.add_if_not_exist("Python")
    files = []
    for i in range(3):
        file = NewFileInRequest(
            title=f"test title {i}",
            content=f"test content {i}",
            language_id=language_id,
            visibility=FileVisibility.PUBLIC,
        )
        response = client.post("/api/file/new", content=file.json())
        file_uid = NewFileCreatedResponse.parse_raw(response.content).uid

        response = client.get(f"/api/file/{file_uid}")
        loaded_file = json.loads(response.content)
        files.append(loaded_file)
    response = client.get("/api/top", params={'offset': 0, 'limit': 10})
    assert response.status_code == 200
    assert json.loads(response.content) == files


def test_top_with_marks(client: TestClient, language_repo: LanguageRepository) -> None:
    response = client.get("/api/top", params={'offset': 0, 'limit': 0})
    assert response.status_code == 200
    assert json.loads(response.content) == []

    language_id = language_repo.add_if_not_exist("Python")
    files = []
    for i in range(3):
        file = NewFileInRequest(
            title=f"test title {i}",
            content=f"test content {i}",
            language_id=language_id,
            visibility=FileVisibility.PUBLIC,
        )
        response = client.post("/api/file/new", content=file.json())
        file_uid = NewFileCreatedResponse.parse_raw(response.content).uid

        response = client.get(f"/api/file/{file_uid}")
        loaded_file = json.loads(response.content)
        files.append(loaded_file)
    response = client.get("/api/top", params={'offset': 0, 'limit': 10})
    assert response.status_code == 200
    assert json.loads(response.content) == files

    file_uid = files[-1]['uid']
    mark = NewMarkRequest(file_uid=file_uid, new_mark=FileMark.STAR)
    response = client.put(f"/api/file/{file_uid}/mark", content=mark.json())
    assert response.status_code == 200

    files[-1]['mark'] = 'star'
    files[-1]['star_count'] = 1
    files = files[-1:] + files[:-1]
    response = client.get("/api/top", params={'offset': 0, 'limit': 10})
    assert response.status_code == 200
    assert json.loads(response.content) == files


def test_top_private(client: TestClient, language_repo: LanguageRepository) -> None:
    response = client.get("/api/top", params={'offset': 0, 'limit': 0})
    assert response.status_code == 200
    assert json.loads(response.content) == []

    language_id = language_repo.add_if_not_exist("Python")
    file = NewFileInRequest(
        title="test title", content="test content", language_id=language_id, visibility=FileVisibility.PRIVATE
    )
    response = client.post("/api/file/new", content=file.json())
    assert response.status_code == 201

    response = client.get("/api/top", params={'offset': 0, 'limit': 0})
    assert response.status_code == 200
    assert json.loads(response.content) == []
