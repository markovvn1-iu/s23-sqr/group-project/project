import pytest
from pydantic import BaseModel

from yacsp.utils.str_enum_with_id import StrEnumWithId


class MyEnum(StrEnumWithId):
    AAA = "bbb", 0
    VALUE1 = "value1", 1
    VALUE2 = "value2", 5


class MyModel(BaseModel):
    enum: MyEnum


def test_enum_basic_usage() -> None:
    assert (MyEnum.AAA.value, MyEnum.AAA.id) == ("bbb", 0)
    assert (MyEnum.VALUE1.value, MyEnum.VALUE1.id) == ("value1", 1)
    assert (MyEnum.VALUE2.value, MyEnum.VALUE2.id) == ("value2", 5)
    assert list(MyEnum) == [MyEnum.AAA, MyEnum.VALUE1, MyEnum.VALUE2]


def test_enum_from() -> None:
    assert MyEnum("bbb") == MyEnum.AAA
    assert MyEnum("value1") == MyEnum.VALUE1
    assert MyEnum["AAA"] == MyEnum.AAA
    assert MyEnum["VALUE1"] == MyEnum.VALUE1
    assert MyEnum.from_id(0) == MyEnum.AAA
    assert MyEnum.from_id(1) == MyEnum.VALUE1
    assert MyEnum.from_id(5) == MyEnum.VALUE2


def test_enum_error_usage() -> None:
    with pytest.raises(KeyError):
        MyEnum.from_id(2)


def test_pydantic() -> None:
    model = MyModel(enum=MyEnum.AAA)
    json = model.json()
    assert json == '{"enum": "bbb"}'
    MyModel.parse_raw(json) == model
