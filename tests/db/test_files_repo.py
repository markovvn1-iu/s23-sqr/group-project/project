import pytest

from yacsp.db.exceptions import NotFoundRepoException
from yacsp.db.repositories import FilesRepository, LanguageRepository, MarksRepository
from yacsp.models import FileMark, FileVisibility, Language, NewFileInRequest, UserMark


async def test_add_get_single_file(files_repo: FilesRepository, language_repo: LanguageRepository) -> None:
    language_id = language_repo.add_if_not_exist("Python")
    file_id = files_repo.save_file(
        NewFileInRequest(
            title="Test file",
            content="Test file content",
            language_id=language_id,
            visibility=FileVisibility.PUBLIC,
        )
    )
    fileRes = files_repo.get_file_for_response(file_id, "new_uid", 2)
    assert fileRes.uid == "new_uid"
    assert fileRes.title == "Test file"
    assert fileRes.content == "Test file content"
    assert fileRes.language == Language(id=1, name="Python")
    assert fileRes.visibility == FileVisibility.PUBLIC
    assert fileRes.mark == FileMark.NO_STAR
    assert fileRes.star_count == 0


async def test_get_unexistent_file(files_repo: FilesRepository) -> None:
    with pytest.raises(NotFoundRepoException):
        files_repo.get_file_for_response(1, "", 2)


async def test_get_top_files(
    files_repo: FilesRepository, language_repo: LanguageRepository, marks_repo: MarksRepository
) -> None:
    language_id = language_repo.add_if_not_exist("Python")
    files_ids = []
    for i in range(2):
        file_id = files_repo.save_file(
            NewFileInRequest(
                title=f"Test file {i}",
                content=f"Test file content {i}",
                language_id=language_id,
                visibility=FileVisibility.PUBLIC,
            )
        )
        files_ids.append(file_id)
    top_files = files_repo.get_top_files(1)
    assert len(top_files) == 2
    assert [i.id for i in top_files] == files_ids

    marks_repo.set(UserMark(user_id=1, file_id=files_ids[-1], mark=FileMark.STAR))
    top_files = files_repo.get_top_files(1)

    assert len(top_files) == 2
    assert [i.id for i in top_files] == list(reversed(files_ids))

    files_repo.save_file(
        NewFileInRequest(
            title="Test file private",
            content="Test file content private",
            language_id=language_id,
            visibility=FileVisibility.PRIVATE,
        )
    )

    top_files = files_repo.get_top_files(1)
    assert len(top_files) == 2
    assert [i.id for i in top_files] == list(reversed(files_ids))
