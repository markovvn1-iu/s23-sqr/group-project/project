from yacsp.db.repositories import LanguageRepository


async def test_check_empty(language_repo: LanguageRepository) -> None:
    assert len(language_repo.get_all()) == 0


async def test_add_single_language(language_repo: LanguageRepository) -> None:
    assert len(language_repo.get_all()) == 0
    language_repo.add_if_not_exist("Python")
    res = language_repo.get_all()
    assert len(res) == 1
    assert res[0].name == "Python"


# async def test_add_multiple_languages(language_repo: LanguageRepository) -> None:
#     assert len(language_repo.get_all()) == 0
#     language_repo.add_if_not_exist("Python")
#     language_repo.add_if_not_exist("C++")
#     language_repo.add_if_not_exist("C++")
#     language_repo.add_if_not_exist("Python")
#     res = language_repo.get_all()
#     assert len(res) == 2
#     assert res[0].name == "Python"
#     assert res[1].name == "C++"
