from yacsp.db.repositories import FilesRepository, LanguageRepository, MarksRepository
from yacsp.models import FileMark, FileVisibility, NewFileInRequest, UserMark


async def test_set_mark(
    files_repo: FilesRepository, language_repo: LanguageRepository, marks_repo: MarksRepository
) -> None:
    user_id = 1
    language_id = language_repo.add_if_not_exist("Python")
    file_id = files_repo.save_file(
        NewFileInRequest(
            title="Test file",
            content="Test file content",
            language_id=language_id,
            visibility=FileVisibility.PUBLIC,
        )
    )
    file_uid = "new_uid"
    res = files_repo.get_file_for_response(file_id=file_id, file_uid=file_uid, user_id=user_id)
    assert res.mark == FileMark.NO_STAR
    assert res.star_count == 0

    marks_repo.set(UserMark(user_id=user_id, file_id=file_id, mark=FileMark.STAR))
    res = files_repo.get_file_for_response(file_id=file_id, file_uid=file_uid, user_id=user_id)
    assert res.mark == FileMark.STAR
    assert res.star_count == 1

    marks_repo.set(UserMark(user_id=user_id, file_id=file_id, mark=FileMark.NO_STAR))
    res = files_repo.get_file_for_response(file_id=file_id, file_uid=file_uid, user_id=user_id)
    assert res.mark == FileMark.NO_STAR
    assert res.star_count == 0
